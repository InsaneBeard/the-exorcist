package test.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.commitStatusPublisher
import jetbrains.buildServer.configs.kotlin.buildSteps.PowerShellStep
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell

object TestPowerShellScripts : BuildType({
    name = "Test - PowerShell Scripts"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    steps {
        powerShell {
            name = "Test"
            edition = PowerShellStep.Edition.Core
            formatStderrAsError = true
            scriptMode = file {
                path = ".deployment/tests/_test_all.ps1"
            }
        }
        powerShell {
            name = "Analyze"
            edition = PowerShellStep.Edition.Core
            formatStderrAsError = true
            scriptMode = file {
                path = ".deployment/tests/_analyze_all.ps1"
            }
        }
    }

    failureConditions {
        executionTimeoutMin = 10
    }

    features {
        commitStatusPublisher {
            publisher = gitlab {
                gitlabApiUrl = "https://gitlab/api/v4"
                accessToken = "credentialsJSON:e82baf61-022c-4bbf-a9fe-7450a39b35e9"
            }
        }
    }

    dependencies {
        snapshot(build.buildTypes.BuildSwitchX) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }
    
    disableSettings("RUNNER_93", "TEMPLATE_RUNNER_1")
})
