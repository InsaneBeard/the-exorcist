# GirlData class - Holds the data for a girl card
extends Reference

signal changed()

# Base data
var id 		= "" # Identifies the girl card, unique in the library
var tags   	= [] # Lists additional specifiers for the girl card
var images 	= {} # Lists the different image used to represent this girl card
var texts  	= {} # Lists the different texts displayed on the girl card

func duplicate():
	var copy = get_script().new()
	
	copy.id			= id
	copy.tags     	= tags.duplicate()
	copy.images   	= images.duplicate()
	copy.texts    	= texts.duplicate()
	
	return copy

# Marks the data as changed for widgets to update their content
func mark_changed():
	emit_signal("changed")