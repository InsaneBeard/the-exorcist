extends Control

var _branch = null
var skill_bar_widget = preload("res://game/widgets/skill_bar/skill_bar.tscn")
var empty_control_kludge = Control.new()
var skill_dim = 72
var spacing = 20

# Called when the node enters the scene tree for the first time.
func _ready():
	redraw()

func load_skills(branch):
	_branch = branch
	$name_label.text = _branch.name
	$description_label.text = _branch.description
	$quote_label.text = _branch.quote
	var skill_length = 0
	for tier in range(SkillLibrary.max_tier):
		skill_length = max(_add_tier(tier), skill_length)
	$skillbar_scroll.rect_size = Vector2(skill_length * skill_dim + spacing * 2, SkillLibrary.max_tier * (skill_dim + spacing))
	redraw()

func _add_tier(tier):
	var count = $skillbar_scroll/skillbar_container.get_child_count()
	var skill_bar = skill_bar_widget.instance()
	if $skillbar_scroll/skillbar_container.get_child(count-1) != empty_control_kludge:
		$skillbar_scroll/skillbar_container.add_child(skill_bar)
		$skillbar_scroll/skillbar_container.add_child(empty_control_kludge)
	else:
		$skillbar_scroll/skillbar_container.remove_child(empty_control_kludge)
		$skillbar_scroll/skillbar_container.add_child(skill_bar)
		$skillbar_scroll/skillbar_container.add_child(empty_control_kludge)
	var skill_length = skill_bar.load_tiered_branch_skills(_branch.name, tier)
	return skill_length

func redraw():
	$skillbar_scroll/skillbar_container.set("custom_constants/vseparation", skill_dim/2)
	for child in $skillbar_scroll/skillbar_container.get_children():
		if child != empty_control_kludge:
			child.set_draw_values(skill_dim, spacing)