extends Control

var skill_node_rect_dim = 0
var spacing = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	draw()

func _get_size():
	var count = get_child_count()
	var width = ((skill_node_rect_dim + spacing) * count) + spacing
	var height = skill_node_rect_dim + (spacing * 2)
	return Vector2(width, height)
	
func _get_child_pos(idx):
	var x = spacing + idx * (skill_node_rect_dim + spacing)
	return Vector2(x, spacing)

func set_draw_values(dim, dim_spacing):
	skill_node_rect_dim = dim
	spacing = dim_spacing
	draw()

func draw():
	rect_size = _get_size()
	var count = get_child_count()
	for idx in range(count):
		var child = get_children()[idx]
		var pos = _get_child_pos(idx)
		child.rect_position = pos
		child.rect_size = Vector2(skill_node_rect_dim, skill_node_rect_dim)
		
func add_skill(skill_data):
	var scene = SkillLibrary.skill_widget
	var node = scene.instance()
	node.set_skill(skill_data)
	add_child(node)
	draw()