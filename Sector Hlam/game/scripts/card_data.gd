# CardData class - Holds the data for a card
extends Reference

signal changed()

# Base data
var name     	= "" # Identifies the card, unique in the library
var branch 		= "" # Specifies the card's branch
var type     	= "" # Specifies the card's type
var cost		= "" # Specifies card cost
var card_art   	= "" # Relpath to card art
var effects    	= {} # Lists the different effects this card does
var trigger 	= "" # Specifies when the card effects will trigger

# Gameplay data
var player = null # Specifies to which player the card belongs
var origin = null # Specifies from where the card comes (Pile, Hand, ...)

func duplicate():
	var copy = get_script().new()
	
	copy.name		= name
	copy.branch		= branch
	copy.type     	= type
	copy.cost		= cost
	copy.card_art   = card_art
	copy.effects    = effects.duplicate()
	copy.trigger	= trigger
	
	copy.player   	= player
	copy.origin   	= origin
	
	return copy

# Marks the data as changed for widgets to update their content
func mark_changed():
	emit_signal("changed")