# CardLibrary class - Holds all the card loaded from the database
extends Reference

# Imports
var CardData = preload("card_data.gd")

var _cards = {}

# Loads the Library from the given file
func load_from_database(path):
	var file = File.new()
	var err = file.open(path, File.READ)
	if err == OK:
		var json = JSON.parse(file.get_as_text())
		if json.error == OK:
			_load_cards(json.result)
		else:
			printerr("Error while parsing card database: ", json.error_string)
	else:
		printerr("Error while opening card database: ", file.get_error())

# Returns all the cards as an array
func cards():
	return _cards.values()

# Returns the card with the given ID, duplicates the card by default
func card(card_id, duplicate = true):
	if !_cards.has(card_id): return null
	
	if duplicate:
		return _cards[card_id].duplicate()
	else:
		return _cards[card_id]

# Returns the number of cards in the library
func size():
	return _cards.size()

func _load_cards(raw_data):
	var cards = _extract_data(raw_data, "cards", {})
	
	for card_id in cards:
		var card = CardData.new()
		
		card.id       = card_id
		card.branch   = _extract_data(cards[card_id], "branch", "")
		card.type     = _extract_data(cards[card_id], "type", "")
		card.images   = _extract_data(cards[card_id], "images", {})
		card.texts    = _extract_data(cards[card_id], "texts", {})
		
		_cards[card_id] = card


func _extract_data(dict, key, default):
	if dict.has(key):
		return dict[key]
	else:
		return default