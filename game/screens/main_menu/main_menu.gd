extends Panel

# Called when the node enters the scene tree for the first time.
func _ready():
	if not self._saved_session_available():
		$menu_panel/button_continue.disabled = true
	var skill_library = $"/root/SkillLibrary"

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _saved_session_available():
	return false
	
func _open_index_screen():
	get_tree().change_scene("res://game/screens/the_index/the_index.tscn")
	
func _exit():
	get_tree().quit()