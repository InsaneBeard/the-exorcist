package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*

object BuildEventRejector : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Build - Event Rejector"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        text("Build.Project", """src\RejectEvent\RejectEvent.csproj""", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Artifact.Paths", """src\RejectEvent\bin\%Build.Configuration%\%Build.Framework% => EventRejector.zip""", label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    dependencies {
        snapshot(test.buildTypes.TestResults) {
        }
    }
})
