package deployment.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.buildSteps.smbUpload
import jetbrains.buildServer.configs.kotlin.triggers.VcsTrigger
import jetbrains.buildServer.configs.kotlin.triggers.schedule
import jetbrains.buildServer.configs.kotlin.triggers.vcs

object DeploySwitchXToInternalReleaseFolder : BuildType({
    name = "Deploy - SwitchX to Internal Release Folder"

    enablePersonalBuilds = false
    type = BuildTypeSettings.Type.DEPLOYMENT
    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"
    maxRunningBuilds = 1

    params {
        text("File.Installer", "${SignInstallerWithExtendedValidationCertificate.depParamRefs["Artifact.File"]}", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("File.Installer.Current", "<to be replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("File.Archive", "${BuildSwitchX.depParamRefs["Artifact.File"]}", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Directory.CurrentVersion", """d\heliOS\.current""", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Directory.Release", """d\heliOS\${BuildSwitchX.depParamRefs["GitVersion.PreReleaseLabel"]}""", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Prerelease.Label", "%dep.SwitchX_BuildSwitchX.GitVersion.PreReleaseLabel%", display = ParameterDisplay.HIDDEN, allowEmpty = false)
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        smbUpload {
            name = "Copy installer and archive to internal release folder"
            targetUrl = "%Directory.Release%"
            username = """dbs.local\teamcity"""
            password = "credentialsJSON:ec71ec62-410d-4d3d-9cce-07dd43584ea7"
            sourcePath = """
                %File.Archive%
                %File.Installer%
            """.trimIndent()
        }
        powerShell {
            name = "Change file name to neutral current name"
            formatStderrAsError = true
            scriptMode = file {
                path = """.deployment\change_filename_to_neutral_current_name.ps1"""
            }
            scriptArgs = """
                -Path %File.Installer%
                -PrereleaseLabel "%Prerelease.Label%"
                -TeamCityParameterName File.Installer.Current
            """.trimIndent()
        }
        smbUpload {
            name = "Overwrite current installer"
            targetUrl = "%Directory.CurrentVersion%"
            username = """dbs.local\teamcity"""
            password = "credentialsJSON:ec71ec62-410d-4d3d-9cce-07dd43584ea7"
            sourcePath = "%File.Installer.Current%"
        }
    }

    triggers {
        vcs {
            quietPeriodMode = VcsTrigger.QuietPeriodMode.USE_DEFAULT
            branchFilter = """
                +:master
                +:support/*
            """.trimIndent()
        }
        schedule {
            schedulingPolicy = cron {
                hours = "6/6"
                timezone = "Europe/Berlin"
            }
            branchFilter = """
                +:<default>
                +:release/*
                +:hotfix/*
            """.trimIndent()
            triggerBuild = always()
        }
    }

    dependencies {
        snapshot(DeployRegisterOnGitLab) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
        dependency(SignInstallerWithExtendedValidationCertificate) {
            snapshot {
                onDependencyFailure = FailureAction.FAIL_TO_START
            }

            artifacts {
                artifactRules = "%File.Installer%"
            }
        }
        artifacts(build.buildTypes.BuildSwitchX) {
            artifactRules = "%File.Archive%"
        }
    }
})
