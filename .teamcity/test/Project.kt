package test

import test.buildTypes.*
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.CustomChart
import jetbrains.buildServer.configs.kotlin.CustomChart.*
import jetbrains.buildServer.configs.kotlin.Project
import jetbrains.buildServer.configs.kotlin.buildTypeCustomChart

object Project : Project({
    id("test")
    name = "test"

    buildType(TestOvernight)
    buildType(TestPowerShellScripts)
    buildType(TestResults)
    buildType(TestCodeInspections)
    buildType(TestUnitTests)

    features {
        buildTypeCustomChart {
            id = "PROJECT_EXT_9"
            title = "Inspections"
            seriesTitle = "Serie"
            format = CustomChart.Format.TEXT
            series = listOf(
                Serie(title = "Number of Inspection Warnings", key = SeriesKey.INSPECTION_WARNINGS),
                Serie(title = "Number of Inspection Errors", key = SeriesKey.INSPECTION_ERRORS)
            )
        }
    }
    buildTypesOrder = arrayListOf(TestCodeInspections, TestUnitTests, TestPowerShellScripts, TestResults, TestOvernight)
})
