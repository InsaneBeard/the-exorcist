package deployment.buildTypes

import build.buildTypes.BuildReleaseNotes
import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.PowerShellStep
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell

object DeployRegisterOnGitLab : BuildType({
    name = "Deploy - Version to GitLab"

    enablePersonalBuilds = false
    type = BuildTypeSettings.Type.DEPLOYMENT
    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"
    maxRunningBuilds = 1

    params {
        checkbox("DryRun", "", label = "Dry run", description = "If checked, the web-request will be skipped",
                  checked = "-WhatIf")
        password("GitLab.AccessToken", "credentialsJSON:d53aa249-baa4-46cc-9823-60d93c3dc051", label = "GitLab access token", display = ParameterDisplay.HIDDEN, readOnly = true)
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        powerShell {
            edition = PowerShellStep.Edition.Core
            formatStderrAsError = true
            scriptMode = script {
                content = """
                    ${'$'}Changelog = @"
                    ${BuildReleaseNotes.depParamRefs["CRN.Changelog"]}
                    "@
                    
                    .deployment/add_version_to_gitlab.ps1 `
                    -ProjectUri "${BuildSwitchX.depParamRefs["vcsroot.url"]}" `
                    -CommitSha "${BuildSwitchX.depParamRefs["build.vcs.number"]}" `
                    -PreReleaseLabel "${BuildSwitchX.depParamRefs["GitVersion.PreReleaseLabel"]}" `
                    -Version "${BuildSwitchX.depParamRefs["GitVersion.SemVer"]}" `
                    -Changelog ${'$'}Changelog `
                    -AccessToken "%GitLab.AccessToken%"
                """.trimIndent()
            }
        }
    }

    dependencies {
        snapshot(build.buildTypes.BuildReleaseNotes) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
        snapshot(SignInstallerWithExtendedValidationCertificate) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
        snapshot(test.buildTypes.TestResults) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }
})
