package test.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.NUnitStep
import jetbrains.buildServer.configs.kotlin.buildSteps.nunit

object TestOvernight : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Test - Overnight"
    description = "Runs long running tests on latest development"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        text("Build.Project", "switchX.sln", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
    }

    steps {
        nunit {
            name = "Unit Test"
            id = "RUNNER_109"
            nunitVersion = NUnitStep.NUnitVersion.NUnit_2_6_3
            nunitPath = "%teamcity.tool.NUnit.Console.DEFAULT%"
            runtimeVersion = NUnitStep.RuntimeVersion.v4_0
            includeTests = """**\bin\*\*Tests*.dll"""
            excludeCategories = "-LongRunning"
            reduceTestFeedback = true
            runProcessPerAssembly = true
            coverage = dotcover {
                assemblyFilters = """
                    -:DbsMathematics
                    -:HtmlAgilityPack
                    -:Npgsql
                    -:SharpDX*
                """.trimIndent()
            }
        }
    }

    failureConditions {
        executionTimeoutMin = 90
    }

    dependencies {
        snapshot(build.buildTypes.BuildSwitchX) {
        }
        snapshot(TestCodeInspections) {
        }
        snapshot(TestUnitTests) {
        }
    }
})
