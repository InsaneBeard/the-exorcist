extends Node

func load_from_file(file_path, callback: FuncRef):
	var file = File.new()
	var err = file.open(file_path, File.READ)
	if err == OK:
		var json = JSON.parse(file.get_as_text())
		if json.error == OK:
			callback.call_func(json.result)
		else:
			printerr("Error while parsing skill file: ", json.error_string)
	else:
		printerr("Error while opening skill file: ", file.get_error())

func load_from_database(resource_path, callback: FuncRef):
	print("Working with path: ", resource_path)
	var dir = Directory.new()
	if dir.open(resource_path) == OK:
        dir.list_dir_begin()
        var file_name = dir.get_next()
        while (file_name != ""):
            if not dir.current_is_dir() and file_name.get_extension() == "json":
                print("Found json file: " + file_name)
                load_from_file(resource_path+file_name, callback)
            file_name = dir.get_next()
        dir.list_dir_end()
	else:
        print("An error occurred when trying to access the path: ", resource_path)