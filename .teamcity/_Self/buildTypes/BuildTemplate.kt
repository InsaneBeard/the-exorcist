package _Self.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.dotnetBuild
import jetbrains.buildServer.configs.kotlin.buildSteps.dotnetRestore
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell

object BuildTemplate : Template({
    name = "BuildTemplate"
    description = "Template for any project builds"

    artifactRules = "%Artifact.Paths%"

    params {
        text("env.DOTNET_HOME", """%env.LOCALAPPDATA%\Microsoft\dotnet\""", readOnly = true)
        text("Build.Project", "", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Artifact.Paths", "", label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("env.Git_Branch", "${DslContext.settingsRoot.paramRefs.buildVcsBranch}", readOnly = true, allowEmpty = true)
        select("Build.Configuration", "Release", label = "Build Configuration", description = "Configuration of the build.",
                options = listOf("Release", "Debug"))
        password("env.NuGetTelerikPassword", "credentialsJSON:f8fddd1f-89ed-4be6-bc96-dbce92315002", display = ParameterDisplay.HIDDEN, readOnly = true)
        select("Build.Framework", "net6.0", label = "Build Framework", description = "Framework targeted by the build.", readOnly = true)
        text("teamcity.git.fetchAllHeads", "true", readOnly = true, allowEmpty = true)
        password("env.NuGetDbsPassword", "credentialsJSON:fa0fb59b-506a-4f3b-972f-4395d14706d7", display = ParameterDisplay.HIDDEN, readOnly = true)
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        powerShell {
            name = "Install dotNET"
            id = "INSTALL_DOT_NET"
            formatStderrAsError = true
            scriptMode = file {
                path = """.config\dotnet-install.ps1"""
            }
            scriptArgs = """
                -JSonFile ".\global.json"
                -InstallDir %env.DOTNET_HOME%
            """.trimIndent()
        }
        dotnetRestore {
            name = "Restore"
            id = "TEMPLATE_RUNNER_1"
            projects = "%Build.Project%"
            configFile = "NuGet.config"
            args = "--force"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
        dotnetBuild {
            name = "build"
            id = "RUNNER_93"
            projects = "%Build.Project%"
            configuration = "%Build.Configuration%"
            args = "--warnaserror --no-incremental"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
    }

    failureConditions {
        executionTimeoutMin = 20
    }
})
