package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.commitStatusPublisher
import jetbrains.buildServer.configs.kotlin.buildSteps.dotnetCustom

object BuildSwitchX : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Build - SwitchX"

    params {
        text("Build.Project", """src\SwitchX\SwitchX.csproj""", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Artifact.File", "%Artifact.Name%.zip", label = "Artifact Name", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("Artifact.Paths", """
            src\SwitchX\bin\%Build.Configuration% => %Artifact.File%
            %Artifact.WiX.VersionFile%
        """.trimIndent(), label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("GitVersion.SemVer", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("Artifact.Name", "heliOS_%GitVersion.InformationalVersion%", label = "Artifact Name", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("Artifact.WiX.VersionFile", "GitVersion_WixVersion.wxi", label = "WiX Version File", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("GitVersion.AssemblySemFileVer", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("GitVersion.PreReleaseLabel", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("GitVersion.InformationalVersion", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("GitVersion.VersionSourceSha", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    steps {
        dotnetCustom {
            name = "Restore dotNET Tools"
            id = "RESTORE_DOTNET_TOOLS"
            args = "tool restore"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
        dotnetCustom {
            name = "Execute GitVersion"
            id = "EXECUTE_GIT_VERSION"
            args = "tool run dotnet-gitversion -- /output buildserver /updateprojectfiles *.csproj /updatewixversionfile"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
        stepsOrder = arrayListOf("INSTALL_DOT_NET", "RESTORE_DOTNET_TOOLS", "EXECUTE_GIT_VERSION", "TEMPLATE_RUNNER_1", "RUNNER_93")
    }

    features {
        commitStatusPublisher {
            id = "BUILD_EXT_1"
            publisher = gitlab {
                gitlabApiUrl = "https://gitlab/api/v4"
                accessToken = "credentialsJSON:e82baf61-022c-4bbf-a9fe-7450a39b35e9"
            }
        }
    }
})
