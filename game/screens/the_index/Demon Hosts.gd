extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func load_hosts():
	$Regular.clear_grid()
	$Boss.clear_grid()
	add_regular_hosts()
	add_boss_hosts()
		
func add_regular_hosts():
		for host in HostLibrary.regular().values():
			var host_view = HostLibrary.host_index_widget.instance()
			host_view.set_host(host)
			$Regular.add_element(host_view)
		$Regular.redraw()
			
func add_boss_hosts():
		for host in HostLibrary.boss().values():
			var host_view = HostLibrary.host_index_widget.instance()
			host_view.set_host(host)
			$Boss.add_element(host_view)
		$Boss.redraw()

func _on_Demon_Hosts_resized():
	print("tab rect:", rect_size)
