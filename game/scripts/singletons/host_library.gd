extends Node

var host_index_widget = preload("res://game/widgets/host_index_view/host_index_view.tscn")
var host_class = preload("res://game/scripts/reference_classes/host.gd")
var host_resources_path = "res://external_resources/hosts/"

var _hosts = {"regular": {}, "boss": {}}

# Called when the node enters the scene tree for the first time.
func _ready():
	load_library()
	
func load_library():
	var host_callback = funcref(self, "_load_hosts")
	ReferenceDataLoader.load_from_database(host_resources_path, host_callback)

func _load_hosts(raw_data):
	var hosts = raw_data.get("hosts", {})

	for host_name in hosts.keys():
		var host = host_class.new()

		host.name 		= host_name
		host.title 		= hosts[host_name].get("title", "")
		host.type 		= hosts[host_name].get("type", "")
		if not host.type in ["regular", "boss"]:
			host.type = "regular"
		host.art_dict 	= hosts[host_name].get("art_dict", {})
		
		_hosts[host.type][host.name] = host

func hosts():
	return _hosts
	
func regular():
	return _hosts["regular"]
	
func boss():
	return _hosts["boss"]
	
func merge_host_art_path(path):
	return host_resources_path + path