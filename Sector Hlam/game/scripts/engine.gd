# Engine class - Singleton keeping track of the Engine state
extends Node

# Imports
var CardLibrary 		= preload("res://game/scripts/asset_library.gd")
var CardRng     		= preload("res://game/scripts/rng.gd")
var CardInstance 		= preload("res://game/widgets/widget_card.tscn")
var GirlCardInstance 	= preload("res://game/widgets/widget_girl_card.tscn")

# The Library is created as a singleton
var _library = CardLibrary.new()

# The Master RNG is used to seed other RNG in sub system 
var _master_rng = CardRng.new()

# Intializes CardEngine
func _init():
	_library.load_from_database()

# Returns the Library singleton
func library():
	return _library
	
func cards():
	return _library.cards()

# Returns the Master RNG
func master_rng():
	return _master_rng
	
func card_instance():
	return CardInstance.instance()
	
func girl_card_instance():
	return GirlCardInstance.instance()
