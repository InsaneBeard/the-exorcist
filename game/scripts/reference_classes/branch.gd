extends Reference

# Base data
var name     	= "" # Identifies the branch name
var description = "" # Specifies the branch description
var quote     	= "" # Specifies the branch flavor quote

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.