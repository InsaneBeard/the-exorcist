package deployment.buildTypes

import build.buildTypes.BuildSwitchX
import build.buildTypes.BuildSwitchXInstaller
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell

object SignInstallerWithExtendedValidationCertificate : BuildType({
    name = "Sign - Installer with extended validation (EV) certificate"

    enablePersonalBuilds = false
    artifactRules = "%File.ToSign%"
    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"
    maxRunningBuilds = 1

    params {
        password("certificate.container.password", "credentialsJSON:551a385c-0d4e-44ec-ab44-5d1042a02f61", display = ParameterDisplay.HIDDEN)
        text("Artifact.File", "%File.ToSign%", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
        text("certificate.file", ".certificate/digicert_dynamic_biosensors_gmbh_ev.cer", readOnly = true, allowEmpty = false)
        password("certificate.container.name", "credentialsJSON:797db75b-ea60-484f-838f-6e5abda759a1", display = ParameterDisplay.HIDDEN)
        text("Certificate.SkipSigning", "b", display = ParameterDisplay.HIDDEN)
        text("certificate.cryptographicServiceProvider", "eToken Base Cryptographic Provider", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
        text("File.ToSign", "${BuildSwitchXInstaller.depParamRefs["Artifact.File"]}", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
    }

    vcs {
        root(DslContext.settingsRoot, "+:. => .certificate")
    }

    steps {
        powerShell {
            name = "Sign installer"

            conditions {
                doesNotEqual("Certificate.SkipSigning", "true")
            }
            scriptMode = file {
                path = ".certificate/sign_file_with_ev_certificate.ps1"
            }
            scriptArgs = """
                -certificate_file "%certificate.file%"
                -cryptographic_security_provider "%certificate.cryptographicServiceProvider%"
                -certificate_container_name "%certificate.container.name%"
                -certificate_container_password "%certificate.container.password%"
                -file ".\%File.ToSign%"
                -signtool_path "%system.teamcity.sign.tool.path%"
            """.trimIndent()
        }
    }

    dependencies {
        dependency(build.buildTypes.BuildSwitchXInstaller) {
            snapshot {
                onDependencyFailure = FailureAction.FAIL_TO_START
            }

            artifacts {
                artifactRules = "%File.ToSign%"
            }
        }
    }

    requirements {
        equals("system.teamcity.sign.support.ev", "TRUE")
    }
})
