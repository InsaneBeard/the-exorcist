extends Panel

# Called when the node enters the scene tree for the first time.
func _ready():
	$skill_center_container/skill.init()
	self.fade_in()
	
func init(desc):
	$skill_description.text = desc
	$skill_description.hint_tooltip = desc

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func fade_out():
	var tween = $fade_out_tween
	var particles = $particle_effect
	tween.interpolate_property(self, "modulate:a", 1, 0, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
	particles.emitting = true
	tween.start()
	yield(tween, "tween_all_completed")

func fade_in():
	var tween = $fade_out_tween
	tween.interpolate_property(self, "modulate:a", 0, 1, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_all_completed")


func _on_skill_pressed():
	self.fade_out()
	#self.get_parent().remove_child(self)
