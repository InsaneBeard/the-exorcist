extends Button

var action_cost setget action_cost_set

func action_cost_set(new_value: int):
	action_cost = new_value
	if action_cost != 0:
		$action_cost/action_label.text = str(action_cost)
		$mana_cost/mana_label.hint_tooltip = "This skill will cost you %s action points.".format(str(action_cost))
		$action_cost.visible = true
	else:
		$action_cost.visible = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func init(action_cost = 0):
	self.action_cost_set(action_cost)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
