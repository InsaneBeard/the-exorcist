extends Control

var skill_node_rect_dim = 72
var spacing = skill_node_rect_dim/8

# Called when the node enters the scene tree for the first time.
func _ready():
	$skill_container.set_draw_values(skill_node_rect_dim, spacing)
	redraw()

func set_draw_values(skill_dim, spacing):
	skill_node_rect_dim = skill_dim
	spacing = spacing
	redraw()

func redraw():
	var dim = skill_node_rect_dim + spacing * 2
	var skill_count = $skill_container.get_child_count()
	rect_size = Vector2(skill_node_rect_dim * skill_count, dim)
	$tier_label.rect_size = Vector2(skill_node_rect_dim, dim)
	$skill_container.rect_position = Vector2(dim-spacing, 0)

func add_skill(skill_data):
	$skill_container.add_skill(skill_data)
	
func load_tiered_branch_skills(branch, tier):
	var skills = SkillLibrary.skills(branch, tier)
	for skill_data in skills.values():
		$skill_container.add_skill(skill_data)
	$tier_label.text = "Tier " + str(tier)
	return len(skills)