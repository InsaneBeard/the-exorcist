extends TabContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func load_branches():
	for child in get_children():
		self.remove_child(child)
	for branch in SkillLibrary.branches().values():
		add_branch(branch)
		
func add_branch(branch):
		var scroll = ScrollContainer.new()
		var skill_branch = SkillLibrary.branch_widget.instance()
		scroll.name = branch.name
		scroll.scroll_vertical_enabled = true
		skill_branch.load_skills(branch)
		scroll.add_child(skill_branch)
		add_child(scroll)