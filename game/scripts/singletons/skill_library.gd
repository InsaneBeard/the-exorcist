extends Node

var skill_class = load("res://game/scripts/reference_classes/skill.gd")
var branch_class = load("res://game/scripts/reference_classes/branch.gd")
var skill_widget = preload("res://game/widgets/skill_view/skill_view.tscn")
var branch_widget = preload("res://game/widgets/skill_branch/skill_branch.tscn")
var branch_resources_path = "res://external_resources/branches/"
var skill_resources_path = "res://external_resources/skills/"
var skill_art_path = self.skill_resources_path + "art/"

var max_tier = 4

var _skills = {}
var _branches = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	load_library()
	
func load_library():
	var branch_callback = funcref(self, "_load_branches")
	var skill_callback = funcref(self, "_load_skills")
	ReferenceDataLoader.load_from_database(branch_resources_path, branch_callback)
	ReferenceDataLoader.load_from_database(skill_resources_path, skill_callback)

func _load_branches(raw_data):
	var branches = raw_data.get("branches", {})

	for branch_name in branches.keys():
		var branch = branch_class.new()

		branch.name 		= branch_name
		branch.description 	= branches[branch_name].get("description", "")
		branch.quote 		= branches[branch_name].get("quote", "")
		
		if !_skills.has(branch_name):
			_skills[branch_name] = {}
		_branches[branch_name] = branch

func _load_skills(raw_data):
	var skills = raw_data.get("skills", {})

	for skill_name in skills.keys():
		var skill = skill_class.new()

		skill.name 		= skill_name
		skill.branch 	= skills[skill_name].get("branch", "")
		skill.tier 		= skills[skill_name].get("tier", max_tier)
		skill.type 		= skills[skill_name].get("type", "")
		skill.cost 		= skills[skill_name].get("cost", 0)
		var art 		= skills[skill_name].get("art", "")
		skill.art 		= skill_art_path + art if not art.empty() else ""
		skill.effects 	= skills[skill_name].get("effects", {})
		
		if !_skills.has(skill.branch):
			print("Could not find branch ", skill.branch)
		_skills[skill.branch][skill.name] = skill

func branches():
	return _branches

func skills(branch, tier = null):
	if tier == null:
		return _skills[branch]
	else:
		var filtered_skills = {}
		for skill in _skills[branch].values():
			if skill.tier == tier:
				filtered_skills[skill.name] = skill
		return filtered_skills

func skill(skill_name, branch, duplicate = true):
	if !_skills[branch].has(skill_name): return null

	if duplicate:
		return _skills[branch][skill_name].duplicate()
	else:
		return _skills[branch][skill_name]