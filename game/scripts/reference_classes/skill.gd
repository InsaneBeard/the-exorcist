extends Reference

signal changed()

# Base data
var name     	= "" # Identifies the skill, unique in the branch
var branch 		= "" # Specifies the skill's branch
var tier		= 1  # Specifies the skill tier
var type     	= "" # Specifies the skill's type
var cost		= "" # Specifies skill cost
var art   		= "" # Relpath to skill art
var effects    	= {} # Lists the different effects this skill executes

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func duplicate():
	var copy = get_script().new()
	
	copy.name		= name
	copy.branch		= branch
	copy.tier		= tier
	copy.type     	= type
	copy.cost		= cost
	copy.art   		= art
	copy.effects    = effects.duplicate()
	
	return copy

func mark_changed():
	emit_signal("changed")