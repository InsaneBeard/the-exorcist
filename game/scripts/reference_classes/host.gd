extends Reference

signal changed()

# Base data
var name     		= "" # Identifies the host, unique in the library
var title 			= "" # Specifies the host title
var type     		= "" # Specifies the host's type (regular/elite)
var art_dict   		= {} # Dictionary of relpaths to different art assets

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func duplicate():
	var copy = get_script().new()
	
	copy.name		= name
	copy.title		= title
	copy.type     	= type
	copy.art_dict   = art_dict.duplicate()
	
	return copy

func mark_changed():
	emit_signal("changed")
	
func portrait_art():
	var portrait_subpath = art_dict.get("portrait", "")
	return HostLibrary.merge_host_art_path(portrait_subpath)