extends Button

var _skill = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_skill(skill_data):
	_skill = skill_data
	_update_view()
	
func _update_view():
	icon = load(_skill.art)
	hint_tooltip = _skill.name


func _on_info_button_pressed():
	$info_panel.popup()
