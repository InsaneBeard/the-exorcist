package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.dotnetCustom
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell

object BuildReleaseNotes : BuildType({
    name = "Build - ReleaseNotes"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        text("env.DOTNET_HOME", """%env.LOCALAPPDATA%\Microsoft\dotnet\""", readOnly = true)
        text("CRN.Changelog", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        powerShell {
            name = "Install dotNET"
            formatStderrAsError = true
            scriptMode = file {
                path = """.config\dotnet-install.ps1"""
            }
            scriptArgs = """
                -JSonFile ".\global.json"
                -InstallDir %env.DOTNET_HOME%
            """.trimIndent()
        }
        dotnetCustom {
            name = "Restore dotNET Tools"
            args = "tool restore"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
        dotnetCustom {
            name = "Execute Changelog generation"
            args = "tool run changelog -- %teamcity.build.checkoutDir%"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
    }

    dependencies {
        snapshot(BuildSwitchX) {
        }
    }
})
