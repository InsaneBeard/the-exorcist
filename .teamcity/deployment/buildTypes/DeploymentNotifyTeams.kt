package deployment.buildTypes

import build.buildTypes.BuildReleaseNotes
import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.triggers.finishBuildTrigger

object DeploymentNotifyTeams : BuildType({
    name = "Notify - Teams"

    enablePersonalBuilds = false
    type = BuildTypeSettings.Type.DEPLOYMENT
    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"
    maxRunningBuilds = 1

    params {
        text("Teams.ChannelHook", "<To Be Replaced>", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("Teams.ChannelHook.Release", "https://dynamicbiosensors.webhook.office.com/webhookb2/a9cd40d2-7ed4-490e-ba52-8b650cc2acbd@31de98b2-cd54-460a-a192-c9686706b00a/IncomingWebhook/6b8a31379e444101a3f65cf5feafbe14/34246947-9e8d-427f-a38b-3eb2a9f04c0b", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
        text("Changelog.Appendix", "*Please use **only** the newest version!*", label = "Release notes appendix", description = "A short message which will be displayed after the release notes (supports Markdown)", display = ParameterDisplay.PROMPT, allowEmpty = true)
        checkbox("DryRun", "${'$'}False", label = "Dry run", description = "If checked, the web-request will be skipped",
                  checked = "${'$'}True", unchecked = "${'$'}False")
        text("Teams.ChannelHook.DryRun", "https://dynamicbiosensors.webhook.office.com/webhookb2/e0813f6d-c6fa-4050-aef0-ff471a516205@31de98b2-cd54-460a-a192-c9686706b00a/IncomingWebhook/9290e0c4bd6a4b168b7fc178754c2d36/34246947-9e8d-427f-a38b-3eb2a9f04c0b", label = "Dry run teams channel hook", description = "Specify channel to which notification should be posted", allowEmpty = true)
        text("Teams.ChannelHook.Beta", "https://dynamicbiosensors.webhook.office.com/webhookb2/a9cd40d2-7ed4-490e-ba52-8b650cc2acbd@31de98b2-cd54-460a-a192-c9686706b00a/IncomingWebhook/6b8a31379e444101a3f65cf5feafbe14/34246947-9e8d-427f-a38b-3eb2a9f04c0b", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
        text("Teams.ChannelHook.Alpha", "https://dynamicbiosensors.webhook.office.com/webhookb2/e0813f6d-c6fa-4050-aef0-ff471a516205@31de98b2-cd54-460a-a192-c9686706b00a/IncomingWebhook/ad942fb7360c4db1a53654d67a04c653/34246947-9e8d-427f-a38b-3eb2a9f04c0b", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        powerShell {
            name = "Determine Teams.ChannelHook"
            formatStderrAsError = true
            scriptMode = script {
                content = """
                    ${'$'}ErrorActionPreference = "Stop"
                    
                    ${'$'}pre_release_tag= "${BuildSwitchX.depParamRefs["GitVersion.PreReleaseLabel"]}"
                    
                    if (%DryRun%) {                                     # Dry run
                     ${'$'}teams_channel_hook="%Teams.ChannelHook.DryRun%"
                    } elseif (${'$'}pre_release_tag -eq "") {                 # Proper release
                     ${'$'}teams_channel_hook="%Teams.ChannelHook.Release%"
                    } elseif (${'$'}pre_release_tag -eq "beta"){             # Beta release
                     ${'$'}teams_channel_hook="%Teams.ChannelHook.Beta%"
                    } elseif (${'$'}pre_release_tag -eq "alpha") {             # Develop release
                     ${'$'}teams_channel_hook="%Teams.ChannelHook.Alpha%"
                    } else {                                             # Feature branch release
                     ${'$'}teams_channel_hook=""
                    }
                    
                    Write-Host "##teamcity[setParameter name='Teams.ChannelHook' value='${'$'}teams_channel_hook']"
                """.trimIndent()
            }
        }
        powerShell {
            formatStderrAsError = true
            scriptMode = script {
                content = """
                    ${'$'}Changelog = @"
                    ${BuildReleaseNotes.depParamRefs["CRN.Changelog"]}
                    "@
                    
                    .deployment/post_release_message_to_teams.ps1 `
                    -Title "New heliOS ${BuildSwitchX.depParamRefs["GitVersion.InformationalVersion"]} is available" `
                    -Summary "New heliOS ${BuildSwitchX.depParamRefs["GitVersion.InformationalVersion"]} is available" `
                    -VersionNumber "${BuildSwitchX.depParamRefs["GitVersion.InformationalVersion"]}  (${BuildSwitchX.depParamRefs["GitVersion.SemVer"]})" `
                    -ChangeLog "${'$'}Changelog" `
                    -Appendix "%Changelog.Appendix%" `
                    -ThemeColor "009fe3" `
                    -WebhookUrl "%Teams.ChannelHook%"
                """.trimIndent()
            }
        }
    }

    triggers {
        finishBuildTrigger {
            buildType = "${DeployVersionToYouTrack.id}"
            successfulOnly = true
            branchFilter = "+:*"
        }
    }

    dependencies {
        snapshot(DeployVersionToYouTrack) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }
})
