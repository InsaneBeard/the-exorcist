# https://github.com/JetBrains/teamcity-docker-images
FROM jetbrains/teamcity-agent:2023.05

# Run as root or ur mom gay
USER root

# Add PowerShell
RUN apt-get update \
    && apt-get install -y wget apt-transport-https software-properties-common
RUN wget -q "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb" \
    && dpkg -i packages-microsoft-prod.deb \
    && rm packages-microsoft-prod.deb
RUN apt-get update \
    && apt-get install -y powershell

# Install Azure CLI - https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt#option-1-install-with-one-command
RUN curl -sL https://aka.ms/InstallAzureCLIDeb | bash

# Add netcat (to check port connectivity)
RUN apt-get update \
    && apt-get install -y netcat-openbsd

# Add chili-publish/editor-sdk and FFMpegCore dependencies for integration tests
RUN apt-get update \
    && apt-get install -y libgtk-3-0 libglfw3-dev libgles2-mesa-dev libegl1 xvfb \
    && apt-get install -y ffmpeg libgdiplus

# Add global dotnet tools to the PATH
ENV PATH /root/.dotnet/tools:$PATH