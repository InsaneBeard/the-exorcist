package deployment.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.PowerShellStep
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.triggers.finishBuildTrigger

object DeployVersionToYouTrack : BuildType({
    name = "Deploy - Version to YouTrack"

    enablePersonalBuilds = false
    type = BuildTypeSettings.Type.DEPLOYMENT
    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"
    maxRunningBuilds = 1

    params {
        checkbox("DryRun", "", label = "Dry run", description = "If checked, the web-request will be skipped",
                  checked = "-WhatIf")
        password("YouTrack.AuthenticationToken", "credentialsJSON:61236313-03dd-4d8f-95ea-1d79ddd8f240", display = ParameterDisplay.HIDDEN)
        text("Youtrack.VersionId", "72-2", description = """Id of youtrack version (can be retrieved using "http://youtrack.dbs.local:8080/api/admin/customFieldSettings/bundles/version?fields=id,name")""", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
    }

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        powerShell {
            edition = PowerShellStep.Edition.Core
            formatStderrAsError = true
            scriptMode = file {
                path = """.deployment\add_version_to_youtrack.ps1"""
            }
            scriptArgs = """
                -YoutrackUrl "%youtrack_url%"
                -YoutrackVersionId "%Youtrack.VersionId%"
                -PreReleaseLabel "${BuildSwitchX.depParamRefs["GitVersion.PreReleaseLabel"]}"
                -SemanticVersion "${BuildSwitchX.depParamRefs["GitVersion.InformationalVersion"]}"
                -AccessToken "%YouTrack.AuthenticationToken%"
                %DryRun%
            """.trimIndent()
        }
    }

    triggers {
        finishBuildTrigger {
            buildType = "${DeploySwitchXToInternalReleaseFolder.id}"
            successfulOnly = true
            branchFilter = "+:*"
        }
    }

    dependencies {
        snapshot(DeploySwitchXToInternalReleaseFolder) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
        snapshot(test.buildTypes.TestResults) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }
})
