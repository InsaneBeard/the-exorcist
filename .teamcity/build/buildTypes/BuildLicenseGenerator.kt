package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.triggers.finishBuildTrigger
import test.buildTypes.TestResults

object BuildLicenseGenerator : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Build - License Generator"
    description = "Builds the heliOS license generator"

    params {
        text("Build.Project", """src\LicenseGenerator\LicenseGenerator.csproj""", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Artifact.Paths", """src\LicenseGenerator\bin\%Build.Configuration%\%Build.Framework% => LicenseGenerator.zip""", label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    triggers {
        finishBuildTrigger {
            id = "TRIGGER_11"
            buildType = "${TestResults.id}"
            successfulOnly = true
            branchFilter = "+:master"
        }
    }

    dependencies {
        snapshot(test.buildTypes.TestResults) {
            onDependencyFailure = FailureAction.CANCEL
            onDependencyCancel = FailureAction.CANCEL
        }
    }

    cleanup {
        baseRule {
            artifacts(builds = 5)
        }
    }
})
