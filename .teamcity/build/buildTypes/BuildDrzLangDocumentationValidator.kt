package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.triggers.finishBuildTrigger
import test.buildTypes.TestResults

object BuildDrzLangDocumentationValidator : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Build - DrzLang Documentation Validator"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        text("Build.Project", """src\DocumentationValidator\DocumentationValidator.csproj""", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        text("Artifact.Paths", """src\DocumentationValidator\bin\%Build.Configuration% => DocumentationValidator.zip""", label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    triggers {
        finishBuildTrigger {
            id = "TRIGGER_10"
            buildType = "${TestResults.id}"
        }
    }

    dependencies {
        snapshot(test.buildTypes.TestResults) {
        }
    }
})
