package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.NuGetInstallerStep
import jetbrains.buildServer.configs.kotlin.buildSteps.nuGetInstaller
import jetbrains.buildServer.configs.kotlin.buildSteps.powerShell
import jetbrains.buildServer.configs.kotlin.buildSteps.script

object BuildSwitchXInstaller : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Build - SwitchX Installer"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        text("Artifact.Name", "${BuildSwitchX.depParamRefs["Artifact.Name"]}", label = "Artifact Name", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("Build.Project", """src\SwitchX\SwitchX.csproj""", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
        password("Certificate.Password", "credentialsJSON:da59050a-0be2-4fa3-a102-d0a18350bf91", display = ParameterDisplay.HIDDEN)
        text("Artifact.File", "%Artifact.Name%.msi", label = "Artifact Name", display = ParameterDisplay.HIDDEN, allowEmpty = true)
        text("Artifact.Paths", """src\SwitchXInstaller\bin\%Build.Configuration%\%Artifact.File%""", label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    steps {
        powerShell {
            name = "Set release specific values for WiX installer"
            id = "RUNNER_131"
            formatStderrAsError = true
            workingDir = ".deployment"
            scriptMode = file {
                path = """.deployment\set_upgrade_code_for_WiX_installer.ps1"""
            }
            scriptArgs = """
                -PreReleaseLabel "${BuildSwitchX.depParamRefs["GitVersion.PreReleaseLabel"]}"
                -InformationalVersion "${BuildSwitchX.depParamRefs["GitVersion.InformationalVersion"]}"
                -AssemblyFileVersion "${BuildSwitchX.depParamRefs["GitVersion.AssemblySemFileVer"]}"
            """.trimIndent()
        }
        nuGetInstaller {
            name = "Restore WiX NuGet packages"
            id = "RUNNER_27"
            toolPath = "%teamcity.tool.NuGet.CommandLine.DEFAULT%"
            projects = "switchX.sln"
            updatePackages = updateParams {
                mode = NuGetInstallerStep.UpdateMode.PackagesConfig
            }
        }
        script {
            name = "Build Installer (temporary alternative until .NET runner works reliably)"
            id = "RUNNER_132"
            workingDir = ".deployment"
            scriptContent = """"%MSBuildTools16.0_x86_Path%\MSBuild.exe" %teamcity.build.checkoutDir%\src/SwitchXInstaller/SwitchXInstaller.wixproj /t:Rebuild /p:Configuration=%Build.Configuration%"""
            formatStderrAsError = true
        }
    }

    dependencies {
        dependency(BuildSwitchX) {
            snapshot {
                onDependencyFailure = FailureAction.FAIL_TO_START
            }

            artifacts {
                id = "ARTIFACT_DEPENDENCY_28"
                artifactRules = """
                    *.zip!** => src\SwitchX\bin\%Build.Configuration%
                    ${BuildSwitchX.depParamRefs["Artifact.WiX.VersionFile"]}
                """.trimIndent()
            }
        }
    }

    requirements {
        exists("DotNetFramework3.5_x86", "RQ_1")
    }
    
    disableSettings("RUNNER_93")
})
