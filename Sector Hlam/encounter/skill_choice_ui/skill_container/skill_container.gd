extends Container

var radius = 500
var angle_start = 45
var angle_stop = 135

# Called when the node enters the scene tree for the first time.
func _ready():
	var scene = preload("res://encounter/skill_choice_card/skill_choice_card.tscn")
	var cards_amount = RandomNumberGenerator.new().randi_range(0, 6)
	for i in cards_amount:
		self.add_child(scene.instance())
	self.sort_children()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func sort_children():
	var count = self.get_child_count()
	var step = (self.angle_stop - self.angle_start) / (count - 1)
	var angle = self.angle_start
	for child in self.get_children():
		var child_desc = "This card is placed at %s angle, aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa!".format(angle)
		child.init(child_desc)
		self.place_child(child, angle)
		angle += step
		
func place_child(child, angle):
	var radian = deg2rad(angle)
	var pos_x = (self.radius * cos(radian)) - 100
	var pos_y = -self.radius * sin(radian)
	if pos_y < 0:
		pos_y -= 80
	else:
		pos_y += 80
	var pos_vector = Vector2(pos_x, pos_y)
	self.fit_child_in_rect(child, Rect2(pos_vector, Vector2(160, 200)))
	print(child.rect_position, child.rect_size)
	