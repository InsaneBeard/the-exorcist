package test.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.commitStatusPublisher
import jetbrains.buildServer.configs.kotlin.triggers.vcs

object TestResults : BuildType({
    name = "Test - Tests for each Push"

    type = BuildTypeSettings.Type.COMPOSITE
    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    vcs {
        root(DslContext.settingsRoot)

        showDependenciesChanges = true
    }

    triggers {
        vcs {
        }
    }

    features {
        commitStatusPublisher {
            publisher = gitlab {
                gitlabApiUrl = "https://gitlab/api/v4"
                accessToken = "credentialsJSON:e82baf61-022c-4bbf-a9fe-7450a39b35e9"
            }
        }
    }

    dependencies {
        snapshot(TestCodeInspections) {
        }
        snapshot(TestUnitTests) {
        }
    }
})
