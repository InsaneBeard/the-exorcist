package _Self

import _Self.buildTypes.*
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.Project
import jetbrains.buildServer.configs.kotlin.projectFeatures.YouTrackIssueTracker
import jetbrains.buildServer.configs.kotlin.projectFeatures.youtrack

object Project : Project({

    template(BuildTemplate)

    features {
        youtrack {
            id = "PROJECT_EXT_7"
            displayName = "YouTrack"
            host = "http://youtrack.dbs.local:8080/"
            userName = "kroener"
            password = "credentialsJSON:fed2686a-b5ea-4399-9e16-07ac932473cf"
            projectExtIds = "SWX"
            accessToken = ""
            authType = YouTrackIssueTracker.AuthType.USERNAME_AND_PASSWORD
        }
    }

    cleanup {
        baseRule {
            history(days = 14)
            artifacts(days = 7)
        }
    }
    subProjectsOrder = arrayListOf(RelativeId("build"), RelativeId("test"), RelativeId("deployment"))

    subProject(test.Project)
    subProject(build.Project)
    subProject(deployment.Project)
    subProject(Playground.Project)
})
