extends Panel

var _host = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func set_host(host_data):
	_host = host_data
	_update_view()
	
func _update_view():
	$host_portrait.texture = load(_host.portrait_art())
	$host_name_label.text = _host.name
	$host_title_label.text = _host.title