package test.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.commitStatusPublisher
import jetbrains.buildServer.configs.kotlin.buildSteps.dotnetTest

object TestUnitTests : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Test - UnitTests"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        text("Build.Project", "switchX.sln", label = "Build Project", description = "Specify paths to projects and solutions. [Wildcards](https://www.jetbrains.com/help/teamcity/2020.1/?Wildcards) are supported.", display = ParameterDisplay.HIDDEN, allowEmpty = false)
    }

    steps {
        dotnetTest {
            name = "dotnet test"
            id = "RUNNER_1"
            filter = "TestCategory!=LongRunning"
            configuration = "%Build.Configuration%"
            args = "--warnaserror"
            param("dotNetCoverage.dotCover.home.path", "%teamcity.tool.JetBrains.dotCover.CommandLineTools.DEFAULT%")
        }
    }

    failureConditions {
        executionTimeoutMin = 10
    }

    features {
        commitStatusPublisher {
            id = "BUILD_EXT_1"
            publisher = gitlab {
                gitlabApiUrl = "https://gitlab/api/v4"
                accessToken = "credentialsJSON:e82baf61-022c-4bbf-a9fe-7450a39b35e9"
            }
        }
    }

    dependencies {
        snapshot(build.buildTypes.BuildSwitchX) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }
    
    disableSettings("RUNNER_93", "TEMPLATE_RUNNER_1")
})
