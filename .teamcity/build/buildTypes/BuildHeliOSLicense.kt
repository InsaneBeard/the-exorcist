package build.buildTypes

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildSteps.exec
import jetbrains.buildServer.configs.kotlin.failureConditions.BuildFailureOnText
import jetbrains.buildServer.configs.kotlin.failureConditions.failOnText

object BuildHeliOSLicense : BuildType({
    name = "Build - heliOS License"
    description = "Generates new heliOS license"

    enablePersonalBuilds = false
    artifactRules = """LicenseGenerator\*L%build.counter%-*.lic"""
    buildNumberPattern = "L%build.counter%-%License.Company%"
    maxRunningBuilds = 1

    params {
        checkbox("Feature.Basic.AssayDevelopment", "--assay-development-basic", label = "Basic assay development", display = ParameterDisplay.PROMPT,
                  checked = "--assay-development-basic")
        checkbox("Feature.Advanced.DataAnalysis", "--data-analysis-advanced", label = "Advanced data analysis", display = ParameterDisplay.PROMPT,
                  checked = "--data-analysis-advanced")
        checkbox("Feature.Advanced.MethodDevelopment", "", label = "Advanced method development", display = ParameterDisplay.PROMPT,
                  checked = "--method-development-advanced")
        text("License.Company", "", label = "Licensee Company", description = "Name of the company the license is issued to.", display = ParameterDisplay.PROMPT, allowEmpty = false)
        checkbox("Feature.Basic.DeviceControl", "--device-control-basic", label = "Basic device control", display = ParameterDisplay.PROMPT,
                  checked = "--device-control-basic")
        text("License.ExpiresInMonths.Updates", "2400", label = "Updates expiry (months)", description = "Months until license to update expires", display = ParameterDisplay.PROMPT,
              regex = """\d+""", validationMessage = "Must be an integer number")
        password("Password.Input", "credentialsJSON:4792c964-a88d-4f06-83dd-0c02f49f6115", label = "Password", display = ParameterDisplay.PROMPT)
        text("License.FileName", "License_%build.number%.lic", label = "License file name", display = ParameterDisplay.HIDDEN, readOnly = true, allowEmpty = false)
        checkbox("Feature.Advanced.AssayDevelopment", "--assay-development-advanced", label = "Advanced assay development", display = ParameterDisplay.PROMPT,
                  checked = "--assay-development-advanced")
        checkbox("Feature.Basic.MethodDevelopment", "--method-development-basic", label = "Basic method development", display = ParameterDisplay.PROMPT,
                  checked = "--method-development-basic")
        checkbox("Feature.Advanced.DeviceControl", "", label = "Advanced device control", display = ParameterDisplay.PROMPT,
                  checked = "--device-control-advanced")
        checkbox("Feature.Basic.DataAnalysis", "--data-analysis-basic", label = "Basic data analysis", display = ParameterDisplay.PROMPT,
                  checked = "--data-analysis-basic")
        password("Password.Reference", "credentialsJSON:0c7ebfee-6334-4c1e-b5a4-937f4fed3d7e", display = ParameterDisplay.HIDDEN, readOnly = true)
        text("License.ExpiresInMonths.Usage", "2400", label = "License expiry (months)", description = "Months until license expires", display = ParameterDisplay.PROMPT,
              regex = """\d+""", validationMessage = "Must be an integer number")
        password("Encryption.PrivateKey", "credentialsJSON:67b7acfd-2025-4cbf-a9cd-670d5b20feee", display = ParameterDisplay.HIDDEN)
        text("License.Email", "", label = "Licensee Contact Email", description = "Email address the licensee can be contacted at.", display = ParameterDisplay.PROMPT, allowEmpty = false)
    }

    steps {
        exec {
            name = "Generate License"

            conditions {
                equals("Password.Input", "%Password.Reference%")
            }
            workingDir = """.\LicenseGenerator"""
            path = "LicenseGenerator.exe"
            arguments = """
                -c "%License.Company%"
                -e "%License.Email%"
                -l %License.ExpiresInMonths.Usage%
                -u %License.ExpiresInMonths.Updates%
                %Feature.Basic.MethodDevelopment%
                %Feature.Basic.AssayDevelopment%
                %Feature.Basic.DeviceControl%
                %Feature.Basic.DataAnalysis%
                %Feature.Advanced.MethodDevelopment%
                %Feature.Advanced.AssayDevelopment%
                %Feature.Advanced.DeviceControl%
                %Feature.Advanced.DataAnalysis%
                -p "%Encryption.PrivateKey%"
                -f
                > "%License.FileName%"
            """.trimIndent()
            formatStderrAsError = true
        }
    }

    failureConditions {
        failOnText {
            conditionType = BuildFailureOnText.ConditionType.CONTAINS
            pattern = """is skipped because of unfulfilled condition: "Password"""
            failureMessage = "Incorrect Password"
            reverse = false
            stopBuildOnFailure = true
        }
    }

    dependencies {
        artifacts(BuildLicenseGenerator) {
            buildRule = lastSuccessful("+:master")
            artifactRules = "LicenseGenerator.zip!** => LicenseGenerator"
        }
    }

    cleanup {
        keepRule {
            id = "KEEP_RULE_1"
            keepAtLeast = allBuilds()
            applyToBuilds {
                inBranches {
                    branchState = active()
                }
                withStatus = successful()
            }
            dataToKeep = everything()
        }
    }
})
