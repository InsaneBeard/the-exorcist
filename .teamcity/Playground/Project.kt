package Playground

import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.Project

object Project : Project({
    id("Playground")
    name = "Playground"
    description = "For CI testing purposes only. No productive build configurations."
})
