package test.buildTypes

import build.buildTypes.BuildSwitchX
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.commitStatusPublisher
import jetbrains.buildServer.configs.kotlin.buildSteps.ReSharperInspections
import jetbrains.buildServer.configs.kotlin.buildSteps.reSharperInspections
import jetbrains.buildServer.configs.kotlin.failureConditions.BuildFailureOnMetric
import jetbrains.buildServer.configs.kotlin.failureConditions.failOnMetricChange

object TestCodeInspections : BuildType({
    templates(_Self.buildTypes.BuildTemplate)
    name = "Test - Code Inspections"

    buildNumberPattern = "${BuildSwitchX.depParamRefs.buildNumber}"

    params {
        checkbox("DryRun", "", label = "Dry run", description = "If checked, the web-request will be skipped",
                  checked = "-dry_run")
        text("Artifact.Paths", "", label = "Artifact Path", display = ParameterDisplay.HIDDEN, allowEmpty = true)
    }

    steps {
        reSharperInspections {
            name = "Code inspections"
            id = "RUNNER_99"
            solutionPath = "switchX.sln"
            cltPath = "%teamcity.tool.jetbrains.resharper-clt.2023.1.2%"
            cltPlatform = ReSharperInspections.Platform.X64
            cltPlugins = "Download ReSharperPlugin.CognitiveComplexity/2023.1.0"
            customSettingsProfilePath = "switchX.sln.DotSettings"
            customCmdArgs = """
                "--dotnetcore=%env.DOTNET_HOME%dotnet.exe"
                "-x=ReSharperPlugin.CognitiveComplexity"
                --build
            """.trimIndent()
        }
    }

    failureConditions {
        failOnMetricChange {
            id = "BUILD_EXT_43"
            metric = BuildFailureOnMetric.MetricType.INSPECTION_ERROR_COUNT
            threshold = 0
            units = BuildFailureOnMetric.MetricUnit.DEFAULT_UNIT
            comparison = BuildFailureOnMetric.MetricComparison.MORE
            compareTo = value()
        }
        failOnMetricChange {
            id = "BUILD_EXT_44"
            metric = BuildFailureOnMetric.MetricType.INSPECTION_WARN_COUNT
            threshold = 0
            units = BuildFailureOnMetric.MetricUnit.DEFAULT_UNIT
            comparison = BuildFailureOnMetric.MetricComparison.MORE
            compareTo = build {
                buildRule = lastSuccessful()
            }
        }
    }

    features {
        commitStatusPublisher {
            id = "BUILD_EXT_1"
            publisher = gitlab {
                gitlabApiUrl = "https://gitlab/api/v4"
                accessToken = "credentialsJSON:e82baf61-022c-4bbf-a9fe-7450a39b35e9"
            }
        }
    }

    dependencies {
        snapshot(build.buildTypes.BuildSwitchX) {
            onDependencyFailure = FailureAction.FAIL_TO_START
        }
    }
    
    disableSettings("RUNNER_93")
})
