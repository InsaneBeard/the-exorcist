# AssetLibrary class - Holds all the assets loaded from the girl/card database
extends Reference

# Imports
var CardData = load("res://game/scripts/card_data.gd")
var card_resources_path = "res://external_resources/cards/"
var card_art_path = self.card_resources_path + "art/"

var _cards = {}

# Loads the Library from the given file
func load_from_file(file_path):
	var file = File.new()
	var err = file.open(file_path, File.READ)
	if err == OK:
		var json = JSON.parse(file.get_as_text())
		if json.error == OK:
			_load_cards(json.result)
		else:
			printerr("Error while parsing card file: ", json.error_string)
	else:
		printerr("Error while opening card file: ", file.get_error())

func load_from_database():
    var dir = Directory.new()
    if dir.open(card_resources_path) == OK:
        dir.list_dir_begin()
        var file_name = dir.get_next()
        while (file_name != ""):
            if not dir.current_is_dir() and file_name.get_extension() == "json":
                print("Found json file: " + file_name)
                load_from_file(card_resources_path+file_name)
            file_name = dir.get_next()
        dir.list_dir_end()
    else:
        print("An error occurred when trying to access the path %s.".format(card_resources_path))

# Returns all the cards as an array
func cards():
	return _cards.values()

# Returns the card with the given ID, duplicates the card by default
func card(card_name, duplicate = true):
	if !_cards.has(card_name): return null

	if duplicate:
		return _cards[card_name].duplicate()
	else:
		return _cards[card_name]

# Returns the number of cards in the library
func size():
	return _cards.size()

func _load_cards(raw_data):
	var cards = _extract_data(raw_data, "cards", {})

	for card_name in cards:
		var card = CardData.new()

		card.name       	= card_name
		card.branch   		= _extract_data(cards[card_name], "branch", "")
		card.type     		= _extract_data(cards[card_name], "type", "")
		card.cost     		= _extract_data(cards[card_name], "cost", 0)
		var card_art_value 	= _extract_data(cards[card_name], "card_art", "")
		card.card_art   	= card_art_path + card_art_value if not card_art_value.empty() else ""
		card.effects    	= _extract_data(cards[card_name], "effects", {})
		card.trigger		= _extract_data(cards[card_name], "trigger", "")

		_cards[card_name] = card


func _extract_data(dict, key, default):
	if dict.has(key):
		return dict[key]
	else:
		return default