package deployment

import deployment.buildTypes.*
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.Project

object Project : Project({
    id("deployment")
    name = "Deploy"

    buildType(DeploymentNotifyTeams)
    buildType(DeployVersionToYouTrack)
    buildType(DeploySwitchXToInternalReleaseFolder)
    buildType(SignInstallerWithExtendedValidationCertificate)
    buildType(DeployRegisterOnGitLab)
    buildTypesOrder = arrayListOf(SignInstallerWithExtendedValidationCertificate, DeployRegisterOnGitLab, DeploySwitchXToInternalReleaseFolder, DeployVersionToYouTrack, DeploymentNotifyTeams)
})
