extends ScrollContainer

var child_size = Vector2(0, 0)
var spacing = 20
var empty_control_kludge = Control.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	set_child_size(Vector2(240, 300))
	redraw()

func set_child_size(size: Vector2):
	child_size = size
	redraw()
	
func add_element(node):
	var count = $container.get_child_count()
	if $container.get_child(count-1) != empty_control_kludge:
		$container.add_child(node)
		$container.add_child(empty_control_kludge)
	else:
		$container.remove_child(empty_control_kludge)
		$container.add_child(node)
		$container.add_child(empty_control_kludge)
	
func clear_grid():
	for child in $container.get_children():
		$container.remove_child(child)

func redraw():
	var cell_width = spacing + child_size.x
	var cell_height = spacing + child_size.y
	#print($container.columns, "||", cell_width, "||", rect_size.x)
	$container.columns = round((rect_size.x - cell_width/2) / cell_width)
	$container.set("custom_constants/hseparation", cell_width)
	$container.set("custom_constants/vseparation", cell_height)
	
func _on_resized():
	redraw()