package build

import build.buildTypes.*
import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.Project

object Project : Project({
    id("build")
    name = "build"

    buildType(BuildReleaseNotes)
    buildType(BuildHeliOSLicense)
    buildType(BuildExpressionTester)
    buildType(BuildSwitchX)
    buildType(BuildDrzLangDocumentationValidator)
    buildType(BuildSwitchXInstaller)
    buildType(BuildLicenseGenerator)
    buildType(BuildEventRejector)
    buildTypesOrder = arrayListOf(BuildSwitchX, BuildSwitchXInstaller, BuildReleaseNotes, BuildDrzLangDocumentationValidator, BuildEventRejector, BuildExpressionTester, BuildLicenseGenerator, BuildHeliOSLicense)
})
