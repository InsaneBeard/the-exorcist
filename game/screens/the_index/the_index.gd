extends Panel

# Called when the node enters the scene tree for the first time.
func _ready():
	$tabs/Skills.load_branches()
	$"tabs/Demon Hosts".load_hosts()
	get_tree().get_root().connect("size_changed", self, "_resize")
	_resize()

func _resize():
	var new_size = get_viewport_rect().size
	rect_size = new_size
	$tabs.rect_size = new_size - Vector2(40, 40)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _open_main_menu():
	get_tree().change_scene("res://game/screens/main_menu/main_menu.tscn")